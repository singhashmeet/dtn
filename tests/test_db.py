import unittest
import json
import os

from store.db import DB


class TestDB(unittest.TestCase):
    def createfile(self, data):
        f = open('test_asset.json', 'w')
        json.dump(data, f)
        f.close()

    def __del__(self):
        os.remove('test_asset.json')

    def test_dirtyload(self):
        data = [{"assetName": "Some Name", "quadKey": "12345"}]
        self.createfile(data)
        db = DB('test_asset.json')
        asset = db.Get("12345")
        self.assertIsNone(asset)

    def test_invalidjson(self):
        data = "invalid_json_data"
        self.createfile(data)
        db = DB('test_asset.json')
        self.assertEqual(db.assets, {})

    def test_get(self):
        data = [{
            "assetName": "Some Name",
            "quadKey": "12345",
            "assetOwner": "Owner"
        }]
        self.createfile(data)
        db = DB('test_asset.json')
        self.assertDictContainsSubset(db.Get("12345"), {
            "assetName": "Some Name",
            "quadKey": "12345",
            "assetOwner": "Owner"
        })

    def test_dupe(self):
        data = [{
            "assetName": "Some Name",
            "quadKey": "12345",
            "assetOwner": "Owner"
        }]
        self.createfile(data)
        db = DB('test_asset.json')
        db.Mark("12345")
        self.assertEqual(db.Dupe("12345"), True)
