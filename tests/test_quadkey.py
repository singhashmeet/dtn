import unittest
from utils.helper import Quadkey


class TestQuadkey(unittest.TestCase):
    def test_clip(self):
        self.assertEqual(Quadkey().clip(12, 100, 11), 11)

    def test_mapsize(self):
        self.assertEqual(Quadkey().mapSize(12), 1048576)

    def test_latLongToPixelXY(self):
        px, py = Quadkey().latLongToPixelXY(8.6972308, -12.2895479, 12)
        self.assertEqual(px, 488492)
        self.assertEqual(py, 498858)

    def test_pixelXYToTileXY(self):
        tx, ty = Quadkey().pixelXYToTileXY(256, 256)
        self.assertEqual(tx, 1)
        self.assertEqual(ty, 1)

    def test_tileXYToQuadKey(self):
        self.assertEqual(Quadkey().tileXYToQuadKey(1098, 1121, 12),
                         '030003201012')

    def test_GetQuadKey(self):
        self.assertEqual(Quadkey().GetQuadKey(8.6972308, -12.2895479),
                         '033321132300')


if __name__ == '__main__':
    unittest.main()