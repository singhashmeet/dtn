import unittest
import json
import os
from dtn.lightning import Lightning
from store.db import DB


class TestLightning(unittest.TestCase):
    def createfile(self, path, data):
        f = open(path, 'w')
        json.dump(data, f)
        f.close()

    def __del__(self):
        os.remove('test_asset.json')

    def test_heartbeat(self):
        data = {
            "flashType": 9,
            "strikeTime": 1446761051071,
            "latitude": 8.7009834,
            "longitude": -12.2791418,
            "peakAmps": 7661,
            "reserved": "000",
            "icHeight": 12583,
            "receivedTime": 1446761063462,
            "numberOfSensors": 5,
            "multiplicity": 1
        }
        self.assertIsNone(Lightning("", None).getAsset(data))

    def test_asset(self):
        lightning_data = {
            "flashType": 1,
            "strikeTime": 1446761047263,
            "latitude": 32.9156667,
            "longitude": -98.2450546,
            "peakAmps": -1422,
            "reserved": "000",
            "icHeight": 19127,
            "receivedTime": 1446761058392,
            "numberOfSensors": 16,
            "multiplicity": 50
        }

        asset_data = [{
            "assetName": "Elease Estates",
            "quadKey": "023112320032",
            "assetOwner": "27359"
        }]
        self.createfile('test_asset.json', asset_data)

        db = DB('test_asset.json')

        self.assertDictContainsSubset(
            Lightning("", db).getAsset(lightning_data), asset_data[0])
