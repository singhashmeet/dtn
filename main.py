import argparse
import signal
import sys
from store.db import DB
from dtn.lightning import Lightning


# signal_handler handles the kill signal and shutdown gracefully
def signal_handler(sig, frame):
    print('Graceful Shutdown!!')
    sys.exit(0)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="""
    This script injests weather lightining data and prints out lightning locations
    """)
    parser.add_argument("--lightning",
                        help="Path to file with lightning inputs")
    parser.add_argument("--assets", help="Path to file with assets inputs")

    args = parser.parse_args()

    # initiate db object
    store = DB(args.assets)
    # initiate lightning listner
    lightning = Lightning(args.lightning, store)

    signal.signal(signal.SIGINT, signal_handler)
    # start listner
    lightning.Start()
