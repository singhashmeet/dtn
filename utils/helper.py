import math


# Quadkey translates latitude and longitude data into quad key at zoom level 12
class Quadkey():
    minLatitude = -85.05112878
    maxLatitude = 85.05112878
    minLongitude = -180
    maxLongitude = 180
    zoom = 12

    def clip(self, n, minVal, maxVal):
        return min(max(n, minVal), maxVal)

    def mapSize(self, zoom):
        return int(256 << zoom)

    def latLongToPixelXY(self, lat, lon, zoom):
        lat = self.clip(lat, self.minLatitude, self.maxLatitude)
        lon = self.clip(lon, self.minLongitude, self.maxLongitude)

        x = (lon + 180) / 360

        sinLat = math.sin(lat * math.pi / 180)

        y = 0.5 - math.log((1 + sinLat) / (1 - sinLat)) / (4 * math.pi)

        mapSize = self.mapSize(zoom)

        pixelX = int(self.clip(x * mapSize + 0.5, 0, mapSize - 1))
        pixelY = int(self.clip(y * mapSize + 0.5, 0, mapSize - 1))
        return pixelX, pixelY

    def pixelXYToTileXY(self, pixelX, pixelY):
        return int(pixelX / 256), int(pixelY / 256)

    def tileXYToQuadKey(self, tileX, tileY, zoom):
        quadkey = ""
        for i in range(zoom, 0, -1):
            value = ord('0')
            mask = 1 << (i - 1)
            if (tileX & mask) != 0:
                value += 1
            if (tileY & mask) != 0:
                value += 2
            quadkey += chr(value)
        return quadkey

    def GetQuadKey(self, lat, lon):
        pixX, pixY = self.latLongToPixelXY(lat, lon, self.zoom)
        tilX, tilY = self.pixelXYToTileXY(pixX, pixY)
        return self.tileXYToQuadKey(tilX, tilY, self.zoom)
