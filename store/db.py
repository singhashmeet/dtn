import json


# DB impliments the db mock interface for the system.
# It handles the assets in memory
class DB():
    def __init__(self, assets):
        self.MustKeys = ["assetName", "quadKey", "assetOwner"]
        self.assetPath = assets
        self.assets = {}
        self.dupe = {}
        self.__load()

    def __load(self):
        with open(self.assetPath, "r") as fp:
            rawData = fp.read()
            try:
                data = json.loads(rawData)
            except Exception, e:
                return
            if type(data) is list:
                for asset in data:
                    if set(asset.keys()) == set(self.MustKeys):
                        self.assets[asset["quadKey"]] = asset
        return

    def Dupe(self, quadKey):
        return self.dupe.get(quadKey)

    def Get(self, quadKey):
        return self.assets.get(quadKey)

    def Mark(self, quadKey):
        self.dupe[quadKey] = True
        return
