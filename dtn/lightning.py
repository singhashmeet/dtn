import json
import sys
from utils.helper import Quadkey


# Lightning contaings handling for event data
class Lightning:
    def __init__(self, lightiningPath, store):
        self.lightning_path = lightiningPath
        self.store = store
        self.File = None
        self.__loadFile()

    def __del__(self):
        if self.File:
            self.File.close()

    def __loadFile(self):
        try:
            f = open(self.lightning_path, 'r')
            self.File = f
        except:
            pass

    # __read detects valid input and tries to retrieve data
    def __read(self):
        text = ''
        if self.File:
            text = self.File.readline()
        else:
            text = sys.stdin.readline()
        try:
            return json.loads(text)
        except:
            return None

    # getAsset takes and filters the lightning data against the store
    def getAsset(self, data):
        if data.get('flashType', 9) <= 1:
            lat = data.get('latitude')
            lon = data.get('longitude')
            quadKey = Quadkey().GetQuadKey(lat, lon)
            if not self.store.Dupe(quadKey):
                self.store.Mark(quadKey)
                return self.store.Get(quadKey)
        return None

    def Start(self):
        data = self.__read()
        while data:
            asset = self.getAsset(data)
            if asset:
                sys.stdout.write("lightning alert for %s:%s\n" %
                                 (asset["assetOwner"], asset["assetName"]))
            data = self.__read()
