# Lightning event analyser

The system impliment an event driven approach for analysing lightning data.

## Input format
---
The system is capable of reading through a file or standard input. The expected format of input is:

```
{
    "flashType": 1,
    "strikeTime": 1386285909025,
    "latitude": 33.5524951,
    "longitude": -94.5822016,
    "peakAmps": 15815,
    "reserved": "000",
    "icHeight": 8940,
    "receivedTime": 1386285919187,
    "numberOfSensors": 17,
    "multiplicity": 1
}
```

The assets data needs to be loaded through a file (format as provided in the question).

```
[
    {
    "assetName":"Dante Street",
    "quadKey":"023112133033",
    "assetOwner":"6720"
    }
]
```

## Executing
---
`python main.py --assets assets.json`

## Arguments
---
| Flag        | Description                       |
|-------------|-----------------------------------|
| --lightning | path to lightning file (optional) |
| --assets    | path to list of assets            |

## Improvements
- Time complexity of detecting if a strike has occurred for a particular asset is O(K).

- If taken to production the in-memory database has a space complexity of O(N) where N is the number of assets. This can lead to memory over flow in production environments. 

- There should be a TTL implimentation if the system goes to production for the Duplicates as that data can become stale over time.